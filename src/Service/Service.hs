module Service.Service (app) where

import Data.Aeson.Types
    (ToJSONKey(..), toJSONKeyText, ToJSON(..), object, (.=), Value(..), FromJSON(..), withObject, (.:), withText
    , Value(Null)
    )
import Domain.Vote (Vote(..), Voter(..), Candidate(..), ElectionResult(..), CandidateResult(..))
import qualified Domain.Vote as Vote
import Domain.User (User(..), UserType(..), Token(..), Username, FullName(..), Password(..), TokenInfo(..))
import qualified Domain.User as User
import Servant
    (ServerT, Application, Proxy(..), serve, (:<|>)(..), ServantErr, errBody, err401, err403, addHeader
    , serveDirectoryWebApp, hoistServer, Handler(..)
    )
import Servant.API
    ( ReqBody, Get, Post, JSON, (:>), ToHttpApiData(..), FromHttpApiData(..), Headers, Header
    , Header', Raw
    )
import Servant.API.Modifiers (Required)
import Network.Wai.Middleware.Rewrite (rewriteRoot)
import Data.Text.Lazy.Encoding as TLE
import Data.Text.Encoding as TE
import Data.Text.Lazy (fromStrict)
import Control.Newtype (Newtype(..))
import qualified Data.ByteString.Char8 as Char8
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Error.Class (throwError)
import Control.Monad.Except (liftEither)

import Data.User ()
import Data.Vote ()
import Data.App (App, runApp)
import Data.Settings (Settings)

-- NOTE the ordering of the routes in the API types needs to be equal to the ordering of the handlers.
-- TODO add file logging
-- COMBINED API

type API
    = "api" :> (UserAPI :<|> VoteAPI)
    :<|> Raw

apiHandler :: ServerT API (App ServantErr)
apiHandler = (userHandler :<|> voteHandler)
    :<|> serveDirectoryWebApp "static"

api :: Proxy API
api = Proxy

transform :: Settings -> App ServantErr a -> Handler a
transform settings input = do
    result <- liftIO $ runApp settings input
    liftEither result

app :: Settings -> Application
app settings = rewriteRoot "Index.html" $ serve api $ hoistServer api (transform settings) apiHandler

-- USER API

-- | Create `Unit` because by default aeson decodes `()` to an empty array and the front-end expects null.
data Unit = Unit

instance ToJSON Unit where
    toJSON _ = Null

data Login = Login { username :: Username, password :: Password }

instance FromJSON Login where
    parseJSON = withObject "Login" $ \v -> Login <$> v .: "username" <*> v .: "password"

instance ToJSON User where
    toJSON t = object
        [ "username" .= (username :: User -> Username) t
        , "fullName" .= (fullName :: User -> FullName) t
        , "userType" .= userType t
        ]

instance ToJSON UserType where
    toJSON t = case t of
        Admin -> "Admin"
        Normal -> "Normal"

instance ToJSON Username where
    toJSON username = String $ unpack username

instance FromJSON Username where
    parseJSON = withText "Username" $ \t -> pure $ pack t

instance ToJSONKey Username where
    toJSONKey = toJSONKeyText unpack

instance ToJSON FullName where
    toJSON (FullName t) = String t

instance FromJSON Password where
    parseJSON = withText "Password" $ \t -> pure $ Password t

instance FromHttpApiData Token where
    parseQueryParam t = Right $ Token t
    parseHeader b =
        case Char8.breakSubstring name b of
            (_, t) | Char8.null t -> fail "Token not present."
                   | otherwise -> Right $ Token $ TE.decodeUtf8 $ Char8.takeWhile (/= ';') $ Char8.drop nameLength t
        where
            name = "authentication_token="
            nameLength = Char8.length name

instance ToHttpApiData Token where
    toUrlPiece (Token token) = token
    toHeader (Token token) = TE.encodeUtf8 $ "authentication_token=" <> token <> "; HttpOnly; Path=/"

type UserAPI
    = "users" :> Header' '[Required] "Cookie" Token :> Get '[JSON] (Maybe User)
    :<|> "users" :> "login" :> ReqBody '[JSON] Login :> Post '[JSON] (Headers '[Header "Set-Cookie" Token] User)
    :<|> "users" :> "logout" :> Header' '[Required] "Cookie" Token :> Get '[JSON] Unit

userHandler :: ServerT UserAPI (App ServantErr)
userHandler = getAuthenticatedUser
    :<|> login
    :<|> logout

getAuthenticatedUser :: Token -> App ServantErr (Maybe User)
getAuthenticatedUser = User.getAuthenticatedUser

login :: Login -> App ServantErr (Headers '[Header "Set-Cookie" Token] User)
login Login { username, password } = do
    result <- User.login username password
    case result of
        Left (User.Error error') ->
            throwError $ err401 { errBody = TLE.encodeUtf8 $ fromStrict error' }
        Right user@User { tokenInfo = TokenInfo { token } } ->
            pure $ addHeader token user

logout :: Token -> App ServantErr Unit
logout token = do
    User.logout token
    pure Unit

-- VOTE API

instance FromJSON Vote where
    parseJSON = withObject "Vote" $ \v -> Vote
        <$> v .: "candidate1"
        <*> v .: "candidate2"
        <*> v .: "candidate3"

instance ToJSON Voter where
    toJSON t = object
        [ "username" .= (username :: Voter -> Username) t
        ]

instance ToJSON Candidate where
    toJSON t = object
        [ "username" .= (username :: Candidate -> Username) t
        , "fullName" .= (fullName :: Candidate -> FullName) t
        , "motivation" .= motivation t
        , "photoUrl" .= photoUrl t
        ]

instance ToJSON CandidateResult where
    toJSON t = object
        [ "points" .= points t
        , "choice1Votes" .= choice1Votes t
        , "choice2Votes" .= choice2Votes t
        , "choice3Votes" .= choice3Votes t
        ]

instance ToJSON ElectionResult where
    toJSON t = object
        [ "results" .= results t
        , "totalVotes" .= totalVotes t
        ]

type VoteAPI
    = "vote" :> Header' '[Required] "Cookie" Token :> ReqBody '[JSON] Vote :> Post '[JSON] Unit
    :<|> "vote" :> "voter" :> Header' '[Required] "Cookie" Token :> Get '[JSON] Unit
    :<|> "vote" :> "voter" :> Header' '[Required] "Cookie" Token :> ReqBody '[JSON] [Username] :> Post '[JSON] [Voter]
    :<|> "vote" :> "candidates" :> Header' '[Required] "Cookie" Token :> Get '[JSON] [Candidate]
    :<|> "vote" :> "results" :> Header' '[Required] "Cookie" Token :> Get '[JSON] ElectionResult

voteHandler :: ServerT VoteAPI (App ServantErr)
voteHandler = vote
    :<|> validateVoter
    :<|> createVoters
    :<|> getCandidates
    :<|> getElectionResult

getCandidates :: Token -> App ServantErr [Candidate]
getCandidates token = do
    result <- Vote.getCandidates token
    handleVoteError result

validateVoter :: Token -> App ServantErr Unit
validateVoter token = do
    result <- Vote.validateVoter token
    handleVoteError result
    pure Unit

createVoters :: Token -> [Username] -> App ServantErr [Voter]
createVoters token usernames = do
    result <- Vote.createVoters token usernames
    handleVoteError result

vote :: Token -> Vote -> App ServantErr Unit
vote token vote' = do
    result <- Vote.vote token vote'
    handleVoteError result
    pure Unit

getElectionResult :: Token -> App ServantErr ElectionResult
getElectionResult token = do
    result <- Vote.getElectionResult token
    handleVoteError result

handleVoteError :: Either Vote.Error a -> App ServantErr a
handleVoteError (Left (Vote.Error error')) = throwError $ err403 { errBody = TLE.encodeUtf8 $ fromStrict error' }
handleVoteError (Left Vote.NotAuthenticated) = throwError err401
handleVoteError (Right v) = pure v