module Election.Election (Query, election) where
  
import Prelude

import Control.Monad.Error.Class (try)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Symbol (class IsSymbol, SProxy(..))
import Data.Vote (class VoteApi, Candidate(..), getCandidates, validateVoter, vote)
import Data.Vote as Vote
import Effect.Exception (message)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Record (set)
import Style.Alert as Alert
import Type.Row (class Cons)

type State =
  { candidates :: Candidates
  , vote :: Voter
  }

data Candidates
  = Candidates (Array Candidate)
  | NoCandidates Alert.Alert
  | LoadCandidates

mapCandidates :: forall b. (Candidate -> b) -> Candidates -> Array b
mapCandidates f (Candidates c) = map f c
mapCandidates f _ = []

type VoteForm 
  = ( alert :: Alert.Alert, candidate1 :: Maybe String, candidate2 :: Maybe String, candidate3 :: Maybe String )
data Voter 
  = VoteForm (Record VoteForm)
  | CannotVote Alert.Alert
  | LoadVoter

setVote :: forall l v r.  IsSymbol l => Cons l v r VoteForm => SProxy l -> v -> Voter -> Voter
setVote label value (VoteForm vote) = VoteForm $ set label value vote
setVote _ _ vote = vote

fromString :: forall a. a -> (String -> a) -> String -> a
fromString default _ "" = default
fromString _ constructor value = constructor value

data Query a
  = SetCandidate1 String a
  | SetCandidate2 String a
  | SetCandidate3 String a
  | Vote a
  | Init a

election :: forall m. VoteApi m => H.Component HH.HTML Query Unit Void m
election =
  H.lifecycleComponent
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    , initializer: Just $ H.action Init
    , finalizer: Nothing
    }
  where

  initialState :: State
  initialState = 
    { candidates: LoadCandidates
    , vote: LoadVoter
    }

  render :: State -> H.ComponentHTML Query
  render state =
    HH.div_ $
      [ HH.h1_ [ HH.text "Candidates" ] ]
      <> case state.candidates of
          Candidates c -> map candidateCard c
          NoCandidates error -> [ Alert.alert error ]
          LoadCandidates -> [ HH.text "Loading..." ]
      <> [ HH.div [ HP.class_ $ H.ClassName "card m-auto" ]
          [ HH.div [ HP.class_ $ H.ClassName "card-body" ] 
              [ HH.h1 [ HP.class_ $ H.ClassName "card-title" ] [ HH.text "Vote" ]
              , case state.vote of
                  VoteForm { alert } -> HH.form_ 
                    [ Alert.alert alert
                    , candidateInput state "candidate1" "Candidate 1" SetCandidate1
                    , candidateInput state "candidate2" "Candidate 2" SetCandidate2
                    , candidateInput state "candidate3" "Candidate 3" SetCandidate3
                    , HH.button
                        [ HP.type_ HP.ButtonButton
                        , HE.onClick $ HE.input_ Vote
                        , HP.class_ $ H.ClassName "btn btn-primary" 
                        ]
                        [ HH.text "Vote" ]
                    ]
                  CannotVote reason -> Alert.alert reason
                  LoadVoter -> HH.text "Loading..."
              ]
          ]
      ]

  candidateInput state id name query =
    HH.div [ HP.class_ $ H.ClassName "form-group" ] 
      [ HH.label [ HP.for id ] [ HH.text name ]
      , HH.select 
          [ HP.id_ id
          , HP.class_ $ H.ClassName "form-control"
          , HE.onValueChange $ HE.input query 
          ]
          ([ HH.option [ HP.value "" ] [ HH.text "Select a candidate" ] ]
            <> mapCandidates 
                (\(Candidate c) -> HH.option [ HP.value c.username ] [ HH.text c.fullName ]) state.candidates)
      ]

  candidateCard (Candidate candidate) =
    HH.div [ HP.class_ $ H.ClassName "card flex-row mb-3" ]
      [ HH.img 
        [ HP.class_ $ H.ClassName "flex-shrink-0", HP.width 150, HP.src candidate.photoUrl, HP.alt candidate.fullName ]
      , HH.div [ HP.class_ $ H.ClassName "card-body" ] 
        [ HH.h5 [ HP.class_ $ H.ClassName "card-title" ] [ HH.text candidate.fullName ]
        , HH.p [ HP.class_ $ H.ClassName "card-text" ] [ HH.text candidate.motivation ]
        ]
      ]

  eval :: Query ~> H.ComponentDSL State Query Void m
  eval = case _ of
    SetCandidate1 username next -> do
      H.modify_ (\s -> s { vote = setVote (SProxy :: SProxy "candidate1") (fromString Nothing Just username) s.vote })
      pure next
    SetCandidate2 username next -> do
      H.modify_ (\s -> s { vote = setVote (SProxy :: SProxy "candidate2") (fromString Nothing Just username) s.vote })
      pure next
    SetCandidate3 username next -> do
      H.modify_ (\s -> s { vote = setVote (SProxy :: SProxy "candidate3") (fromString Nothing Just username) s.vote })
      pure next
    Vote next -> do
      info <- H.gets _.vote
      case info of
        VoteForm { candidate1: Just candidate1, candidate2: Just candidate2, candidate3: Just candidate3 } -> do
          result <- H.liftAff $ try $ vote $ Vote.Vote { candidate1, candidate2, candidate3 }
          case result of
            Left error -> 
              H.modify_ (\s -> s { vote = setVote (SProxy :: SProxy "alert") (Alert.Danger $ message error) s.vote })
            Right _ ->
              H.modify_ _ { vote = CannotVote $ Alert.Primary "Your vote has been successfully processed." }
        VoteForm _ ->
          H.modify_ 
            (\s -> s { vote = setVote (SProxy :: SProxy "alert") (Alert.Danger "Provide three candidates.") s.vote })
        _ -> 
          pure unit
      pure next
    Init next -> do
      validation <- H.liftAff $ try $ validateVoter
      candidates <- H.liftAff $ try $ getCandidates
      H.modify_ _
        { candidates = case candidates of
            Left error -> NoCandidates $ Alert.Danger $ message error
            Right [] -> NoCandidates $ Alert.Danger "No candidates found."
            Right c -> Candidates c
        , vote = case validation of
            Left error -> CannotVote $ Alert.Danger $ message error
            Right _ -> VoteForm { alert: Alert.None, candidate1: Nothing, candidate2: Nothing, candidate3: Nothing }
        }
      pure next
