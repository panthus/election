module Style.Dropdown (State, Query, Message(..), isOpen, toggle, selectItem, dropdown, button, link) where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.HTML.Properties.ARIA as ARIA
import Halogen.Query.EventSource as ES
import Web.Event.Event (Event, EventType, preventDefault) as DOM
import Web.Event.EventTarget (addEventListener, eventListener) as DOM
import Web.HTML (window) as DOM
import Web.HTML.HTMLDocument (HTMLDocument, toEventTarget) as DOM
import Web.HTML.Window (document) as DOM
import Web.UIEvent.MouseEvent (toEvent, MouseEvent) as DOM
import Web.UIEvent.MouseEvent.EventTypes (click) as DOM

data State 
  = Closed
  -- | We attach an event handling for close when opening the dropdown. Due to event bubbling the close event will also
  -- | be triggered by the event that caused the open event. To prevent this from closing the dropdown OpenedIgnoreClose
  -- | is introduced.
  | OpenedIgnoreClose
  | Opened

isOpen' :: State -> Boolean
isOpen' Closed = false
isOpen' _ = true

isOpen :: State -> H.ClassName
isOpen Closed = H.ClassName ""
isOpen _ = H.ClassName "show"

type Props item = forall r. Array (H.IProp ("class" :: String, onClick :: DOM.MouseEvent | r) (Query item))

toggle :: forall item. State -> Array H.ClassName -> Props item
toggle state classes =
  [ HP.classes $ [ H.ClassName "dropdown-toggle" ] <> classes
  , ARIA.hasPopup $ show true
  , ARIA.expanded $ show $ isOpen' state
  , HE.onClick $ Just <<< H.action <<< Open <<< DOM.toEvent
  ]

selectItem :: forall item. item -> Array H.ClassName -> Props item
selectItem item classes =
  [ HP.classes $ [ H.ClassName "dropdown-item" ] <> classes, HE.onClick $ HE.input_ $ SelectItem item ]

data Query item a
  = Close DOM.Event (H.SubscribeStatus -> a)
  | Open DOM.Event a
  | SelectItem item a

data Message item = ItemSelected item

type HTML item = H.ComponentHTML (Query item)

dropdown :: forall item m. MonadAff m => (State -> HTML item) -> H.Component HH.HTML (Query item) Unit (Message item) m
dropdown render =
  H.lifecycleComponent
    { initialState: const initialState
    , render: render
    , eval
    , receiver: const Nothing
    , initializer: Nothing
    , finalizer: Nothing
    }
  where

  initialState :: State
  initialState = Closed

  -- TODO add keyboard support
  eval :: (Query item) ~> H.ComponentDSL State (Query item) (Message item) m
  eval = case _ of
    Close event reply -> do
      state <- H.get
      case state of
        OpenedIgnoreClose -> do
          H.modify_ $ const Opened
          pure (reply H.Listening)
        _ -> do
          H.modify_ $ const Closed
          pure (reply H.Done)
    Open event next -> do
      state <- H.get
      case state of
        Closed -> do
          H.liftEffect $ DOM.preventDefault event
          H.modify_ $ const OpenedIgnoreClose
          
          document <- H.liftEffect $ DOM.document =<< DOM.window
          H.subscribe $ ES.eventSource (addListener DOM.click document) (Just <<< H.request <<< Close)
          pure next
        _ ->
          pure next
    SelectItem item next -> do
      H.raise $ ItemSelected item
      pure next

  addListener :: forall a. DOM.EventType -> DOM.HTMLDocument -> (DOM.Event -> Effect a) -> Effect Unit
  addListener eventType document onEvent = do
    listener <- DOM.eventListener onEvent
    DOM.addEventListener eventType listener false (DOM.toEventTarget document)

button :: forall item. Array H.ClassName -> String -> Array (HTML item) -> State -> HTML item
button classes buttonText items state =
  HH.div [ HP.classes [ H.ClassName "btn-group", isOpen state ] ]
    [ HH.button (toggle state ([ H.ClassName "btn" ] <> classes) <> [ HP.type_ HP.ButtonButton ])
        [ HH.text buttonText ]
    , HH.div [ HP.classes [ H.ClassName "dropdown-menu", isOpen state ] ] items
    ]

link :: forall item. Array H.ClassName -> String -> Array (HTML item) -> State -> HTML item
link classes linkText items state =
  HH.div [ HP.classes [ H.ClassName "dropdown", isOpen state ] ]
    [ HH.a (toggle state classes) [ HH.text linkText ]
    , HH.div [ HP.classes [ H.ClassName "dropdown-menu", isOpen state ] ] items
    ]