module Style.Alert (Alert(..), alert) where
  
import Prelude

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Halogen.HTML.Properties.ARIA as ARIA

data Alert
  = None
  | Danger String
  | Primary String

alert :: forall p i. Alert -> H.HTML p i
alert None = HH.text ""
alert (Danger error) = HH.div [ HP.class_ $ H.ClassName "alert alert-danger", ARIA.role "alert" ] [ HH.text error ]
alert (Primary message) =
  HH.div [ HP.class_ $ H.ClassName "alert alert-primary", ARIA.role "alert" ] [ HH.text message ]