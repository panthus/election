module Election.Layout (layout, Query) where

import Prelude

import Control.Monad.Error.Class (try)
import Data.Either (Either(..))
import Data.Either.Nested (Either2)
import Data.Functor.Coproduct.Nested (Coproduct2)
import Data.Maybe (Maybe(..))
import Data.Symbol (class IsSymbol, SProxy(..))
import Data.User (class UserApi, LoginData(..), UserData(..), getAuthenticatedUser, login, logout)
import Data.Vote (class VoteApi)
import Effect.Exception (Error, message)
import Election.Election as EL
import Halogen as H
import Halogen.Component.ChildPath as CP
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.HTML.Properties.ARIA as ARIA
import Record (set)
import Style.Alert (Alert(..), alert)
import Style.Dropdown as SD
import Type.Row (class Cons)
import Web.Event.Event as DOM

type State =
  { user :: User
  , error :: Alert
  , breadcrumbs :: Array Breadcrumb
  }

type LoginForm = ( password :: Maybe String, username :: Maybe String )
data User
  = User UserData
  | LoginForm (Record LoginForm)
  | AutoLoggingIn

setLogin :: forall l r.  IsSymbol l => Cons l (Maybe String) r LoginForm => SProxy l -> String -> User -> User
setLogin label "" (LoginForm user) = LoginForm $ set label Nothing user
setLogin label value (LoginForm user) = LoginForm $ set label (Just value) user
setLogin _ _ user = user

data Breadcrumb
  = Intermediate String String
  | Active String

data Query a
  = SetUsername String a
  | SetPassword String a
  | Login DOM.Event a
  | AutoLogin a
  | Logout a

type ChildQuery = Coproduct2 (SD.Query (Query Unit)) EL.Query
type ChildSlot = Either2 Unit Unit

layout :: forall m. VoteApi m => UserApi m => H.Component HH.HTML Query Unit Void m
layout =
  H.lifecycleParentComponent
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    , initializer: Just $ H.action AutoLogin
    , finalizer: Nothing
    }
  where

  initialState :: State
  initialState = 
    { user: AutoLoggingIn
    , error: None
    , breadcrumbs: [ Active "Election" ]
    }

  render :: State -> H.ParentHTML Query ChildQuery ChildSlot m
  render state =
    HH.main_
      [ HH.nav 
          [ HP.class_ $ H.ClassName "navbar navbar-light bg-white shadow rounded" ] 
          [ HH.nav [ HP.class_ $ H.ClassName "nav-item", ARIA.label "breadcrumb" ] [ breadcrumbs state.breadcrumbs ]
          , userMenu state.user
          ]
      , HH.article [ HP.class_ $ H.ClassName "p-3" ]
          [ alert state.error
          , case state.user of
              LoginForm _ -> loginForm
              AutoLoggingIn -> HH.text "Loading..."
              User user -> HH.slot' CP.cp2 unit EL.election unit absurd
          ]
      ]
    
  userMenu :: User -> H.ParentHTML Query ChildQuery ChildSlot m
  userMenu (User (UserData { fullName })) =
    HH.slot' CP.cp1 unit (SD.dropdown $ userMenu' fullName) unit (\(SD.ItemSelected q) -> Just q)
  userMenu _ =
    HH.span [ HP.class_ $ H.ClassName "navbar-text" ] [ HH.text "Anonymous" ]
    
  userMenu' :: forall p. String -> SD.State -> H.HTML p (SD.Query (Query Unit))
  userMenu' linkText state =
    HH.div [ HP.classes [ H.ClassName "dropdown nav-item", SD.isOpen state ] ]
      [ HH.a (SD.toggle state [ H.ClassName "nav-item nav-link" ]) [ HH.text linkText ]
      , HH.div [ HP.classes [ H.ClassName "dropdown-menu dropdown-menu-right", SD.isOpen state ] ]
          [ HH.a (SD.selectItem (Logout unit) []) [ HH.text "Logout" ] ]
      ]

  breadcrumbs :: forall p i. Array Breadcrumb -> H.HTML p i
  breadcrumbs list =
    HH.ol [ HP.class_ $ H.ClassName "breadcrumb bg-white m-0 p-0" ] (breadcrumb <$> list)
    where
      breadcrumb (Intermediate name url) =
        HH.li [ HP.class_ $ H.ClassName "breadcrumb-item" ] [ HH.a [ HP.href url ] [ HH.text name ] ]
      breadcrumb (Active name) =
        HH.li 
          [ HP.classes [ H.ClassName "breadcrumb-item", H.ClassName "active" ]
          , HP.attr (H.AttrName "aria-current") "page" 
          ]
          [ HH.text name ]

  loginForm :: forall p. H.HTML p Query
  loginForm =
    HH.div [ HP.class_ $ H.ClassName "d-flex" ]
      [ HH.div [ HP.class_ $ H.ClassName "card m-auto" ]
          [ HH.form [ HP.class_ $ H.ClassName "card-body", HE.onSubmit $ HE.input Login ] 
              [ HH.h1 [ HP.class_ $ H.ClassName "card-title" ] [ HH.text "Login" ]
              , HH.div [ HP.class_ $ H.ClassName "form-group" ] 
                  [ HH.label [ HP.for "username" ] [ HH.text "Username" ]
                  , HH.input 
                      [ HP.id_ "username"
                      , HP.class_ $ H.ClassName "form-control"
                      , HE.onValueInput $ HE.input SetUsername 
                      ]
                  ]
              , HH.div [ HP.class_ $ H.ClassName "form-group" ] 
                  [ HH.label [ HP.for "password" ] [ HH.text "Password" ]
                  , HH.input 
                      [ HP.id_ "password"
                      , HP.type_ HP.InputPassword
                      , HP.class_ $ H.ClassName "form-control"
                      , HE.onValueInput $ HE.input SetPassword
                      ]
                  ]
              , HH.button
                  [ HP.type_ HP.ButtonSubmit, HP.class_ $ H.ClassName "btn btn-primary" ]
                  [ HH.text "Login" ]
              ]
          ]
      ]

  handleError 
    :: forall a
    .  Either Error a
    -> (a -> H.ParentDSL State Query ChildQuery ChildSlot Void m Unit)
    -> H.ParentDSL State Query ChildQuery ChildSlot Void m Unit
  handleError (Left error) _ = H.modify_ _ { error = Danger $ message error }
  handleError (Right a) onSuccess = onSuccess a

  eval :: Query ~> H.ParentDSL State Query ChildQuery ChildSlot Void m
  eval = case _ of
    SetUsername username next -> do
      H.modify_ (\u -> u { user = setLogin (SProxy :: SProxy "username") username u.user })
      pure next
    SetPassword password next -> do
      H.modify_ (\u -> u { user = setLogin (SProxy :: SProxy "password") password u.user })
      pure next
    Login event next -> do
      H.liftEffect $ DOM.preventDefault event
      user <- H.gets _.user
      case user of
        LoginForm { username: Just username, password: Just password } -> do
          info <- H.liftAff $ try $ login $ LoginData { username, password }
          handleError info (\i -> H.modify_ _ { user = User i, error = None })
        LoginForm _ ->
          H.modify_ _ { error = Danger "Enter a valid username and password." }
        _ ->
          pure unit
      pure next
    AutoLogin next -> do
      info <- H.liftAff $ try getAuthenticatedUser
      case info of
        Right (Just i) ->
          H.modify_ _ { user = User i }
        _ ->
          H.modify_ _ { user = LoginForm { username: Nothing, password: Nothing } }
      pure next
    Logout next -> do
      result <- H.liftAff $ try logout
      handleError result $ const $ H.modify_ _ { user = LoginForm { username: Nothing, password: Nothing } }
      pure next