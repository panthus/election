module Data.Vote (Candidate(..), Vote(..), class VoteApi, vote, getCandidates, validateVoter) where
  
import Prelude

import Data.Affjax (post, get)
import Data.Argonaut.Core (jsonEmptyObject)
import Data.Argonaut.Decode (class DecodeJson, decodeJson, getField)
import Data.Argonaut.Encode (class EncodeJson, (:=), (~>))
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff)
  
newtype Candidate = Candidate
  { fullName :: String
  , username :: String
  , motivation :: String
  , photoUrl :: String
  }
newtype Vote = Vote
  { candidate1 :: String
  , candidate2 :: String
  , candidate3 :: String
  }

instance decodeJsonCandidate :: DecodeJson Candidate where
  decodeJson json = do
    obj <- decodeJson json
    username <- getField obj "username"
    fullName <- getField obj "fullName"
    motivation <- getField obj "motivation"
    photoUrl <- getField obj "photoUrl"
    pure $ Candidate { username, fullName, motivation, photoUrl }

instance encodeJsonVote :: EncodeJson Vote where
  encodeJson (Vote obj) =  "candidate1" := obj.candidate1
    ~> "candidate2" := obj.candidate2
    ~> "candidate3" := obj.candidate3
    ~> jsonEmptyObject

class (MonadAff m) <= VoteApi m where
  vote :: Vote -> m Unit
  getCandidates :: m (Array Candidate)
  validateVoter :: m Unit

instance voteApi :: VoteApi Aff where
  vote v = post "/api/vote" v
  getCandidates = get "/api/vote/candidates"
  validateVoter = get "/api/vote/voter"