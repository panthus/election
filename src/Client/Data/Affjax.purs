module Data.Affjax (get, post) where

import Prelude

import Affjax as AX
import Affjax.RequestBody as RB
import Affjax.ResponseFormat as RF
import Affjax.StatusCode as SC
import Control.Monad.Error.Class (throwError)
import Control.Monad.Except (runExcept)
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode (class DecodeJson, decodeJson)
import Data.Argonaut.Encode (class EncodeJson, encodeJson)
import Data.Either (Either(..), either)
import Data.Foldable (intercalate)
import Effect.Aff (Aff, error)
import Foreign (readString, renderForeignError)

handle
  :: forall e a. DecodeJson e => DecodeJson a => AX.Response (Either RF.ResponseFormatError Json) -> Aff (Either e a)
handle response =
  case response of
    { status: SC.StatusCode s, body: Right a } | s >= 200 && s < 300 ->
      either (throw s) (Right >>> pure) $ decodeJson a
    { status: SC.StatusCode s, body: Right a } ->
      either (throw s) (Left >>> pure) $ decodeJson a
    { status: SC.StatusCode s, body: Left (RF.ResponseFormatError _ f) } ->
      either (map renderForeignError >>> intercalate "\r\n" >>> throw s) (throw s) $ runExcept $ readString f
  where
    throw code message = throwError $ error $ show code <> ": " <> message

get :: forall a. DecodeJson a => AX.URL -> Aff a
get url = do
  response <- AX.get RF.json url
  handled <- handle response :: Aff (Either Unit a)
  either (const $ throwError $ error "Unexpected custom error.") pure $ handled

post :: forall b a. EncodeJson b => DecodeJson a => AX.URL -> b -> Aff a
post url body = do
  response <- AX.post RF.json url (RB.json $ encodeJson body)
  handled <- handle response :: Aff (Either Unit a)
  either (const $ throwError $ error "Unexpected custom error.") pure $ handled

get' :: forall e a. DecodeJson e => DecodeJson a => AX.URL -> Aff (Either e a)
get' url = do
  response <- AX.get RF.json url
  handle response

post' :: forall b e a. EncodeJson b => DecodeJson e => DecodeJson a => AX.URL -> b -> Aff (Either e a)
post' url body = do
  response <- AX.post RF.json url (RB.json $ encodeJson body)
  handle response