module Data.User (LoginData(..), UserData(..), class UserApi, login, getAuthenticatedUser, logout) where

import Prelude

import Data.Affjax (get, post)
import Data.Argonaut.Core (jsonEmptyObject)
import Data.Argonaut.Decode (class DecodeJson, decodeJson, getField)
import Data.Argonaut.Encode (class EncodeJson, (:=), (~>))
import Data.Maybe (Maybe)
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff)

newtype UserData = UserData { username :: String, fullName :: String }
newtype LoginData = LoginData { username :: String, password :: String }

instance decodeJsonUserData :: DecodeJson UserData where
  decodeJson json = do
    obj <- decodeJson json
    username <- getField obj "username"
    fullName <- getField obj "fullName"
    pure $ UserData { username, fullName }

instance encodeJsonUserData :: EncodeJson LoginData where
  encodeJson (LoginData obj) =  "username" := obj.username
    ~> "password" := obj.password
    ~> jsonEmptyObject

class (MonadAff m) <= UserApi m where
  login :: LoginData -> m UserData
  getAuthenticatedUser :: m (Maybe UserData)
  logout :: m Unit

instance userApi :: UserApi Aff where  
  login body = post "/api/users/login" body
  getAuthenticatedUser = get "/api/users"
  logout = get "/api/users/logout"