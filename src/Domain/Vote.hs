module Domain.Vote
  ( Vote(..)
  , Error(..)
  , Candidate(..)
  , Voter(..)
  , VoteStatus(..)
  , ElectionResult(..)
  , CandidateResult(..)
  , ElectionPeriod(..)
  , vote
  , validateVoter
  , getCandidates
  , getElectionResult
  , createVoters

  , VoteAPI(..)
  ) where

import Domain.User (
  Token(..), Username, FullName, User(..), UserType(..), Time(..), UserAPI(..), getAuthenticatedUser, createUsers)
import Data.Text (Text, intercalate)
import qualified Data.Text as Text
import Data.List (nub)
import Data.Map.Strict (Map, insertWith, empty)
import Control.Newtype (Newtype(..))
import Data.Time (UTCTime, FormatTime, formatTime, defaultTimeLocale)

data Error
  = Error Text
  | NotAuthenticated

data Vote = Vote
  { candidate1 :: Username
  , candidate2 :: Username
  , candidate3 :: Username
  }

data Candidate = Candidate
  { username :: Username
  , fullName :: FullName
  , motivation :: Text
  , photoUrl :: Text
  }

data VoteStatus = Voted | NotVoted
data Voter = Voter
  { username :: Username
  , status :: VoteStatus
  }

data CandidateResult = CandidateResult
  { points :: Int
  , choice1Votes :: Int
  , choice2Votes :: Int
  , choice3Votes :: Int
  }

data ElectionResult = ElectionResult
  { results :: Map Username CandidateResult
  , totalVotes :: Int
  }

instance Semigroup CandidateResult where
  (<>) (CandidateResult p1 c1v1 c2v1 c3v1) (CandidateResult p2 c1v2 c2v2 c3v2) =
    CandidateResult (p1 + p2) (c1v1 + c1v2) (c2v1 + c2v2) (c3v1 + c3v2)

data ElectionPeriod = ElectionPeriod
    { start :: UTCTime
    , end :: UTCTime
    }

class (Monad m) => VoteAPI m where
  addVote :: Vote -> m ()
  setHasVoted :: Username -> m ()
  getVoter :: Username -> m (Maybe Voter)
  insertVoters :: [Voter] -> m ()
  getNotExistingCandidates :: Vote -> m [Text]
  getAllCandidates :: m [Candidate]
  foldVotes :: a -> (a -> Vote -> IO a) -> m a
  getElectionPeriod :: m ElectionPeriod


getElectionResult :: (Time m, UserAPI m, VoteAPI m) => Token -> m (Either Error ElectionResult)
getElectionResult token = do
  user <- getAuthenticatedUser token
  case user of
    Just User { userType = Admin } -> Right <$> foldVotes emptyResult accumulate
    _ -> pure $ Left NotAuthenticated
  where
    emptyResult = ElectionResult { results = empty, totalVotes = 0 }

    accumulate ElectionResult { results, totalVotes } vote' =
      pure $ ElectionResult { results = processVote results vote', totalVotes = totalVotes + 1 }

    processVote results Vote { candidate1, candidate2, candidate3 } =
      insertWith (<>) candidate1 (CandidateResult 3 1 0 0)
        $ insertWith (<>) candidate2 (CandidateResult 2 0 1 0)
        $ insertWith (<>) candidate3 (CandidateResult 1 0 0 1) results

getCandidates :: (Time m, UserAPI m, VoteAPI m) => Token -> m (Either Error [Candidate])
getCandidates token = do
  user <- getAuthenticatedUser token
  case user of
    Just _ -> Right <$> getAllCandidates
    Nothing -> pure $ Left NotAuthenticated

createVoters :: (Time m, UserAPI m, VoteAPI m) => Token -> [Username] -> m (Either Error [Voter])
createVoters token usernames = do
  user <- getAuthenticatedUser token
  case user of
    Just User { userType = Admin } -> do
      ldapUsers <- getLDAPUsers usernames
      users <- createUsers ldapUsers
      let voters = map (\User { username } -> Voter username NotVoted) users
      insertVoters voters
      pure $ Right voters
    _ -> pure $ Left NotAuthenticated

validateVoter :: (Time m, UserAPI m, VoteAPI m) => Token -> m (Either Error ())
validateVoter token = do
  user <- getAuthenticatedUser token
  case user of
    Just User { username } -> do
      result <- validateVote username Nothing
      pure $ maybe (Right ()) Left result
    Nothing -> pure $ Left NotAuthenticated

validateVote :: (Time m, VoteAPI m) => Username -> Maybe Vote -> m (Maybe Error)
validateVote username vote' = do
  voter <- getVoter username
  ElectionPeriod { start, end } <- getElectionPeriod
  currentTime <- getCurrentTime
  case (status <$> voter, vote') of
    (Nothing, _) ->
      pure $ Just $ Error ("User " <> unpack username <> " is not allowed to vote.")
    (Just _, _) | currentTime < start || currentTime > end ->
      pure $ Just $ Error ("The election period is between " <> formatTime' start <> " and " <> formatTime' end <> ".")
    (Just Voted, _) ->
      pure $ Just $ Error ("User " <> unpack username <> " already placed a vote.")
    (Just NotVoted, Just vote'') -> do
      notExistingCandidates <- getNotExistingCandidates vote''
      let voteList = [ candidate1 vote'', candidate2 vote'', candidate3 vote'' ]
      case notExistingCandidates of
        [] | length (nub voteList) < 3 -> pure $ Just $ Error "Candidate 1, 2 and/or 3 cannot be the same."
        [] | elem username voteList -> pure $ Just $ Error "It is not allowed to vote on yourself."
        [] -> pure Nothing
        list -> pure $ Just $ Error $ "The following candidates do not exist: " <> intercalate ", " list
    (Just NotVoted, Nothing) ->
      pure Nothing

formatTime' :: FormatTime t => t -> Text
formatTime' t = Text.pack $ formatTime defaultTimeLocale "%F, %T (%Z)" t

vote :: (Time m, UserAPI m, VoteAPI m) => Token -> Vote -> m (Either Error ())
vote token vote' = do
  user <- getAuthenticatedUser token
  case user of
    Just User { username } -> do
      result <- validateVote username $ Just vote'
      case result of
        Nothing -> do
          setHasVoted username
          addVote vote'
          pure $ Right ()
        Just error' ->
          pure $ Left error'
    Nothing ->
      pure $ Left NotAuthenticated
