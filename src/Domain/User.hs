{-|
A user is used for authentication.
-}
module Domain.User
    ( User(..)
    , UserType(..)
    , Username
    , FullName(..)
    , Password(..)
    , Error(..)
    , login
    , logout
    , createUsers

    , Token(..)
    , getAuthenticatedUser

    , UserAPI(..)
    , LDAPUser(..)
    , TokenInfo(..)
    , Time(..)
    ) where

import Data.Time (NominalDiffTime, UTCTime(..), addUTCTime)
import Data.ByteString (ByteString)
import Data.Text (Text, toLower)
import qualified Data.Text as Text
import Crypto.Hash (hash, Digest, SHA256)
import Control.Newtype (Newtype(..))

newtype Error = Error Text
newtype Username = Username Text deriving (Eq, Ord)
newtype FullName = FullName Text
newtype Token = Token Text
newtype Password = Password Text

instance Newtype Username Text where
    pack = Username . toLower
    unpack (Username text) = text

data TokenInfo = TokenInfo
    { token :: Token
    , validFrom :: UTCTime
    , duration :: NominalDiffTime
    }

data UserType = Normal | Admin
data User = User
    { username :: Username
    , fullName :: FullName
    , userType :: UserType
    , tokenInfo :: TokenInfo
    }

data LDAPUser = LDAPUser
    { username :: Username
    , fullName :: FullName
    }

class (Monad m) => UserAPI m where
    getUserByToken :: Token -> m (Maybe User)
    getUserByUsername :: Username -> m (Maybe User)
    getLDAPUsers :: [Username] -> m [LDAPUser]
    insertUsers :: [User] -> m ()
    updateUser :: User -> m ()
    ldapLogin :: Username -> Password -> m (Maybe LDAPUser)
    getEntropy :: Int -> m ByteString

class (Monad m) => Time m where
    getCurrentTime :: m UTCTime

getAuthenticatedUser :: (Time m, UserAPI m) => Token -> m (Maybe User)
getAuthenticatedUser token = do
    user <- getUserByToken token
    maybe (pure Nothing) hasValidToken user

createTokenInfo :: (Time m, UserAPI m) => m TokenInfo
createTokenInfo = do
    token <- getEntropy 256
    currentTime <- getCurrentTime
    pure $ TokenInfo (Token $ Text.pack $ show (hash token :: Digest SHA256)) currentTime (fromInteger (60 * 60 * 24))

hasValidToken :: (Time m, UserAPI m) => User -> m (Maybe User)
hasValidToken user@User { tokenInfo = TokenInfo { validFrom, duration } } = do
    currentTime <- getCurrentTime
    if addUTCTime duration validFrom >= currentTime then
        pure $ Just user
    else
        pure Nothing

changeUser :: (Time m, UserAPI m) => User -> LDAPUser -> m User
changeUser user LDAPUser { fullName } = do
    tokenInfo <- createTokenInfo
    let user' = user { tokenInfo = tokenInfo, fullName = fullName }
    updateUser user'
    pure user'

-- | Create users and insert them into the database.
-- | Note that this function has no authentication.
createUsers :: (Time m, UserAPI m) => [LDAPUser] -> m [User]
createUsers ldapUsers = do
    users <- mapM toUser ldapUsers
    insertUsers users
    pure users
    where
        toUser LDAPUser { username, fullName } = do
            tokenInfo <- createTokenInfo
            let user = User username fullName Normal tokenInfo
            pure user

invalidateToken :: UserAPI m => User -> m ()
invalidateToken user@User { tokenInfo } =
    updateUser user { tokenInfo = tokenInfo { duration = 0 } }

login :: (Time m, UserAPI m) => Username -> Password -> m (Either Error User)
login username password = do
    ldapUser <- ldapLogin username password
    case ldapUser of
        Nothing -> pure $ Left $ Error "Invalid credentials."
        Just ldap -> do
            user <- getUserByUsername username
            case user of
                Just user' ->
                    Right <$> changeUser user' ldap
                Nothing ->
                    (\[user'] -> Right user') <$> createUsers [ldap]

logout :: UserAPI m => Token -> m ()
logout token = do
    user <- getUserByToken token
    maybe (pure ()) invalidateToken user
