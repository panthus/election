module Data.App (runApp, AppEnv(..), App) where

import Database.PostgreSQL.Simple 
  (connectPostgreSQL, close, withTransaction, Connection)
import Control.Exception (bracket)
import Control.Monad.Reader (runReaderT, ReaderT)
import Control.Monad.Except (runExceptT, ExceptT)
import Data.Settings (Settings(..), Database(..))

data AppEnv = AppEnv { db :: Connection, settings :: Settings }
type App e = ReaderT AppEnv (ExceptT e IO)

runApp :: Settings -> App e a -> IO (Either e a)
runApp settings@Settings { database = Database { connection } } readerT = bracket
  (connectPostgreSQL connection)
  close
  (\conn -> withTransaction conn (runExceptT $ runReaderT readerT $ AppEnv conn settings))
