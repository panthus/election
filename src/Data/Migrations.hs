module Data.Migrations (runMigrations) where

import Database.PostgreSQL.Simple (execute, execute_, query_, Only(..), Connection)
import Control.Exception (throw)
import Data.Foldable (forM_)
import Data.App (runApp, AppEnv(..))
import Data.Settings (Settings)
import Control.Monad.Reader (ask)
import Control.Monad.IO.Class (liftIO)

data Migration = Migration String (Connection -> IO ())

runMigrations :: Settings -> IO (Either e ())
runMigrations settings =
  runApp settings $ do
    AppEnv { db } <- ask
    _ <- liftIO $ execute_ db "CREATE TABLE IF NOT EXISTS migrationHistory \
    \( id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY NOT NULL \
    \, name TEXT UNIQUE NOT NULL \
    \)"
    history <- liftIO $ query_ db "SELECT name FROM migrationHistory ORDER BY id ASC"
    let (_, migrationsToRun) = foldr determineMigrations (history, []) migrations
    liftIO $ forM_ migrationsToRun (runMigration db)
    where
      determineMigrations m@(Migration name _) (history, migrationsToRun) =
        case history of
          [] -> ([], m : migrationsToRun)
          Only x : xs | x == name -> (xs, migrationsToRun)
                      | otherwise -> throw $ userError 
                        $ "Found migration '" <> x <> "' while migration '" <> name <> "' was expected."

      runMigration conn (Migration name apply) = do
        apply conn
        execute conn "INSERT INTO migrationHistory (name) VALUES (?)" (Only name)

migrations :: [Migration]
migrations = 
  [ Migration "CreateDB1" migrationCreateDB1
  ]

migrationCreateDB1 :: Connection -> IO ()
migrationCreateDB1 conn = do
  _ <- execute_ conn "CREATE TABLE account \
    \( username TEXT PRIMARY KEY NOT NULL \
    \, fullName TEXT NOT NULL \
    \, isAdmin BOOLEAN NOT NULL \
    \, token TEXT UNIQUE NOT NULL \
    \, validFrom TIMESTAMP WITH TIME ZONE NOT NULL \
    \, duration BIGINT NOT NULL \
    \)"
  _ <- execute_ conn "CREATE TABLE candidate \
    \( username TEXT PRIMARY KEY NOT NULL \
    \, motivation TEXT NOT NULL \
    \, photoUrl TEXT NOT NULL \
    \, FOREIGN KEY (username) REFERENCES account (username) ON DELETE NO ACTION ON UPDATE NO ACTION \
    \)"
  _ <- execute_ conn "CREATE TABLE vote \
    \( candidate1 TEXT NOT NULL \
    \, candidate2 TEXT NOT NULL \
    \, candidate3 TEXT NOT NULL \
    \, FOREIGN KEY (candidate1) REFERENCES candidate (username) ON DELETE NO ACTION ON UPDATE NO ACTION \
    \, FOREIGN KEY (candidate2) REFERENCES candidate (username) ON DELETE NO ACTION ON UPDATE NO ACTION \
    \, FOREIGN KEY (candidate3) REFERENCES candidate (username) ON DELETE NO ACTION ON UPDATE NO ACTION \
    \)"
  _ <- execute_ conn "CREATE TABLE voter \
    \( username TEXT PRIMARY KEY NOT NULL \
    \, hasVoted BOOLEAN NOT NULL \
    \, FOREIGN KEY (username) REFERENCES account (username) ON DELETE NO ACTION ON UPDATE NO ACTION \
    \)"
  pure ()