module Data.User () where

import Domain.User
  ( UserAPI(..)
  , Time(..)
  , User(..)
  , UserType(..)
  , Username
  , FullName(..)
  , Password(..)
  , Token(..)
  , LDAPUser(..)
  , TokenInfo(..)
  )
import Data.ByteString (ByteString)
import Data.Time (UTCTime(..))
import qualified Crypto.Random.Entropy as Crypto
import qualified Data.Time as Time
import Database.PostgreSQL.Simple (execute, executeMany, query, (:.)(..), Only(..))
import Database.PostgreSQL.Simple.FromRow (field, FromRow(..))
import Database.PostgreSQL.Simple.ToRow (ToRow(..))
import Database.PostgreSQL.Simple.ToField (ToField(..))
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import qualified Ldap.Client as Ldap
import qualified Ldap.Client.Bind as Ldap
import qualified Ldap.Client.Internal as Ldap
import Control.Exception (throwIO, finally)
import Control.Newtype (Newtype(..))
import Control.Monad.Reader (ask)
import Control.Monad.IO.Class (liftIO)
import Data.Settings (LDAP(..), Settings(..), Bind(..))
import Data.App (App, AppEnv(..))

instance FromRow User where
  fromRow = User
    <$> (pack <$> field)
    <*> (FullName <$> field)
    <*> ((\isAdmin -> if isAdmin then Admin else Normal) <$> field)
    <*> (TokenInfo <$> (Token <$> field) <*> field <*> (toEnum <$> field))

instance ToRow TokenInfo where
  toRow TokenInfo { token = Token token, validFrom, duration } =
    toRow (token, validFrom, fromEnum duration)

instance ToField UserType where
  toField Admin = toField True
  toField Normal = toField False

instance ToRow User where
  toRow User { username, fullName = FullName fullName, userType, tokenInfo } =
    toRow (unpack username, fullName, userType) ++ toRow tokenInfo

instance Time (App e) where
  getCurrentTime :: App e UTCTime
  getCurrentTime = liftIO Time.getCurrentTime

instance UserAPI (App e) where
  getUserByToken :: Token -> App e (Maybe User)
  getUserByToken (Token token) = do
    AppEnv { db } <- ask
    result <-
      liftIO $ query db "SELECT username, fullName, isAdmin, token, validFrom, duration FROM account WHERE token = ?"
      (Only token)
    case result of
      [] -> pure Nothing
      [user] -> pure $ Just user
      (_:_:_) -> liftIO $ throwIO $ userError "Multiple users found where one was expected."

  getUserByUsername :: Username -> App e (Maybe User)
  getUserByUsername username = do
    AppEnv { db } <- ask
    result <-
      liftIO $ query db "SELECT username, fullName, isAdmin, token, validFrom, duration FROM account WHERE username = ?"
      (Only (unpack username))
    case result of
      [] -> pure Nothing
      [user] -> pure $ Just user
      (_:_:_) -> liftIO $ throwIO $ userError "Multiple users found where one was expected."

  getLDAPUsers :: [Username] -> App e [LDAPUser]
  getLDAPUsers usernames = do
    AppEnv
      { settings = Settings { ldap = LDAP { host, port, bind, baseDN, fullNameAttribute, usernameAttribute } } } <- ask
    liftIO $ connectLDAP host port bind $ \ldap ->
      case usernames of
        [] -> pure []
        x : xs -> do
          result <- Ldap.search ldap
            baseDN
            mempty
            (Ldap.Or $ foldl (\f x' -> filter' x' usernameAttribute <> f) (filter' x usernameAttribute) xs)
            [fullNameAttribute, usernameAttribute]
          pure $ foldl
            (\list (Ldap.SearchEntry _ attrs) ->
              case (lookup usernameAttribute attrs, lookup fullNameAttribute attrs) of
                (Just [username], Just [fullName]) ->
                  LDAPUser (pack $ decodeUtf8 username) (FullName $ decodeUtf8 fullName) : list
                _ -> list
            )
            []
            result
    where
      filter' :: Username -> Ldap.Attr -> Ldap.NonEmpty Ldap.Filter
      filter' username attr = pure $ attr Ldap.:= encodeUtf8 (unpack username)

  insertUsers :: [User] -> App e ()
  insertUsers users = do
    AppEnv { db } <- ask
    _ <- liftIO $ executeMany
      db "INSERT INTO account (username, fullName, isAdmin, token, validFrom, duration) VALUES (?, ?, ?, ?, ?, ?)" users
    pure ()

  updateUser :: User -> App e ()
  updateUser User { username, fullName = FullName fullName, tokenInfo } = do
    AppEnv { db } <- ask
    _ <- liftIO $ execute db 
      "UPDATE account SET fullName = ?, token = ?, validFrom = ?, duration = ? WHERE username = ?"
      (Only fullName :. tokenInfo :. Only (unpack username))
    pure ()

  ldapLogin :: Username -> Password -> App e (Maybe LDAPUser)
  ldapLogin username (Password password) = do
    AppEnv
      { settings = Settings { ldap = LDAP { host, port, bind, baseDN, fullNameAttribute, usernameAttribute } } } <- ask
    liftIO $ connectLDAP host port bind $ \ldap -> do
      result <- Ldap.search ldap
        baseDN
        (Ldap.size 1)
        (usernameAttribute Ldap.:= encodeUtf8 (unpack username))
        [fullNameAttribute]
      case result of
        [Ldap.SearchEntry userDN [(_, [fullName])]] -> do
          login <- Ldap.bindEither ldap userDN (Ldap.Password $ encodeUtf8 password)
          pure $ either (const Nothing) (const $ Just $ LDAPUser username (FullName $ decodeUtf8 fullName)) login
        _ ->
          pure Nothing

  getEntropy :: Int -> App e ByteString
  getEntropy amount = liftIO $ Crypto.getEntropy amount

connectLDAP :: Ldap.Host -> Ldap.PortNumber -> Maybe Bind -> (Ldap.Ldap -> IO a) -> IO a
connectLDAP host port bind action = do
  -- Sometimes the ldap connection hangs on Windows, an extra call to unbind seems to fix this...
  -- Probably related to https://ghc.haskell.org/trac/ghc/ticket/7353
  result <- Ldap.with host port (\ldap -> finally (action' ldap) (Ldap.unbindAsync ldap))
  case result of
    Left (Ldap.IOError e) -> throwIO e
    Left (Ldap.ParseError e) -> throwIO e
    Left (Ldap.ResponseError e) -> throwIO e
    Left (Ldap.DisconnectError e) -> throwIO e
    Right result' -> pure result'
  where
    action' ldap = do
      maybe (pure ()) (\Bind { dn, password } -> Ldap.bind ldap dn password) bind
      action ldap