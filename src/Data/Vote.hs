module Data.Vote () where

import Domain.Vote (VoteAPI(..), Vote(..), Candidate(..), Voter(..), VoteStatus(..), ElectionPeriod)
import Domain.User (Username, FullName(..))
import Database.PostgreSQL.Simple (execute, executeMany, query, query_, fold_, Only(..))
import Database.PostgreSQL.Simple.FromRow (field, FromRow(..))
import Database.PostgreSQL.Simple.ToRow (ToRow(..))
import Database.PostgreSQL.Simple.ToField (ToField(..))
import Data.Text (Text)
import Control.Newtype (Newtype(..))
import Control.Monad.Reader (ask)
import Control.Monad.IO.Class (liftIO)
import Data.App (App, AppEnv(..))
import Control.Exception (throwIO)
import Data.Settings (Settings(..), Election(..))

instance ToRow Vote where
  toRow Vote { candidate1, candidate2, candidate3 } =
    toRow (unpack candidate1, unpack candidate2, unpack candidate3)

instance FromRow Vote where
  fromRow = Vote <$> (pack <$> field) <*> (pack <$> field) <*> (pack <$> field)

instance FromRow Candidate where
  fromRow = Candidate
    <$> (pack <$> field)
    <*> (FullName <$> field)
    <*> field
    <*> field

instance FromRow Voter where
  fromRow = Voter
    <$> (pack <$> field)
    <*> ((\bool -> if bool then Voted else NotVoted) <$> field)

instance ToField VoteStatus where
  toField Voted = toField True
  toField NotVoted = toField False

instance ToRow Voter where
  toRow Voter { username, status } = toRow (unpack username, status)

instance VoteAPI (App e) where
  addVote :: Vote -> App e ()
  addVote vote = do
    AppEnv { db } <- ask
    _ <- liftIO $ execute db "INSERT INTO vote (candidate1, candidate2, candidate3) VALUES (?, ?, ?)" vote
    pure ()

  setHasVoted :: Username -> App e ()
  setHasVoted username = do
    AppEnv { db } <- ask
    rowsChanged <- liftIO $ execute db "UPDATE voter SET hasVoted = TRUE WHERE hasVoted = FALSE AND username = ?"
      (Only (unpack username))
    case rowsChanged of
      1 -> pure ()
      _ -> liftIO $ throwIO $ userError "Race condition: it is not allowed to vote twice."

  getVoter :: Username -> App e (Maybe Voter)
  getVoter username = do
    AppEnv { db } <- ask
    result <- liftIO $ query db "SELECT username, hasVoted FROM voter WHERE username = ?"
      (Only (unpack username))
    case result of
      [] -> pure Nothing
      [x] -> pure $ Just x
      (_:_:_) -> liftIO $ throwIO $ userError "Multiple voters found where one was expected."

  insertVoters :: [Voter] -> App e ()
  insertVoters voters = do
    AppEnv { db } <- ask
    _ <- liftIO $ executeMany db "INSERT INTO voter (username, hasVoted) VALUES (?, ?)" voters
    pure ()

  getNotExistingCandidates :: Vote -> App e [Text]
  getNotExistingCandidates vote@Vote { candidate1, candidate2, candidate3 } = do
    AppEnv { db } <- ask
    result <- liftIO $ query db "SELECT username FROM candidate WHERE username IN (?, ?, ?)" vote
    let existing = concat result
    pure $ filter (`notElem` existing) [ unpack candidate1, unpack candidate2, unpack candidate3 ]

  getAllCandidates :: App e [Candidate]
  getAllCandidates = do
    AppEnv { db } <- ask
    liftIO $ query_ db
      "SELECT c.username, u.fullName, c.motivation, c.photoUrl FROM candidate AS c \
      \INNER JOIN account AS u ON u.username = c.username"

  foldVotes :: a -> (a -> Vote -> IO a) -> App e a
  foldVotes value accumulator = do
    AppEnv { db } <- ask
    liftIO $ fold_ db "SELECT candidate1, candidate2, candidate3 FROM vote" value accumulator

  getElectionPeriod :: App e ElectionPeriod
  getElectionPeriod = do
    AppEnv { settings = Settings { election = Election { period } } } <- ask
    pure period