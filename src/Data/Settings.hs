module Data.Settings (Database(..), Bind(..), LDAP(..), Server(..), Election(..), Settings(..)) where

import Data.Text (Text, unpack)
import Data.Text.Encoding (encodeUtf8)
import Data.ByteString (ByteString)
import qualified Ldap.Client as Ldap
import qualified Network.Wai.Handler.WarpTLS as Warp
import qualified Network.Wai.Handler.Warp as Warp
import Data.Yaml (FromJSON(..), (.:), (.:?), Parser, withText, withObject)
import Domain.Vote (ElectionPeriod(..))

data Database = Database { connection :: ByteString }
data Bind = Bind { dn :: Ldap.Dn, password :: Ldap.Password }
data LDAP = LDAP
  { host :: Ldap.Host
  , port :: Ldap.PortNumber
  , bind :: Maybe Bind
  , baseDN :: Ldap.Dn
  , fullNameAttribute :: Ldap.Attr
  , usernameAttribute :: Ldap.Attr
  }
data Server = Server { settings :: Warp.Settings, tlsSettings :: Maybe Warp.TLSSettings }
data Election = Election { period :: ElectionPeriod }
data Settings = Settings
  { database :: Database
  , ldap :: LDAP
  , server :: Server
  , election :: Election
  }

instance FromJSON ByteString where
  parseJSON = withText "ByteString" $ pure . encodeUtf8

instance FromJSON Database where
  parseJSON = withObject "Database" $ \v -> Database
    <$> v .: "connection"

fromHost :: Text -> String -> Parser Ldap.Host
fromHost "Plain" host = pure $ Ldap.Plain host
fromHost "TLS" host = pure $ Ldap.Tls host Ldap.defaultTlsSettings
fromHost security _ = fail $ "Expected Plain or TLS, actual " ++ unpack security

fromBind :: Maybe Text -> Maybe ByteString -> Maybe Bind
fromBind dn pw = Bind <$> (Ldap.Dn <$> dn) <*> (Ldap.Password <$> pw)

instance FromJSON LDAP where
  parseJSON = withObject "LDAP" $ \v -> LDAP
    <$> (v .: "security" >>= \s -> v .: "host" >>= fromHost s)
    <*> (toEnum <$> v .: "port")
    <*> (fromBind <$> v .:? "bindDN" <*> v .:? "bindPassword")
    <*> (Ldap.Dn <$> v .: "baseDN")
    <*> (Ldap.Attr <$> v .: "fullNameAttribute")
    <*> (Ldap.Attr <$> v .: "usernameAttribute")

fromSecurity :: Text -> Maybe String -> Maybe String -> Parser (Maybe Warp.TLSSettings)
fromSecurity "Plain"_ _= pure Nothing
fromSecurity "TLS" c ck = pure $ Warp.tlsSettings <$> c <*> ck
fromSecurity security _ _ = fail $ "Expected Plain or TLS, actual " ++ unpack security

instance FromJSON Server where
  parseJSON = withObject "Server" $ \v -> Server
    <$> ((`Warp.setPort` Warp.defaultSettings) <$> v .: "port")
    <*> (v .: "security" >>= \s -> v .:? "certificate" >>= \c -> v .:? "certificateKey" >>= fromSecurity s c)

instance FromJSON Election where
  parseJSON = withObject "Election" $ \v -> Election
    <$> v .: "period"

instance FromJSON ElectionPeriod where
  parseJSON = withObject "ElectionPeriod" $ \v -> ElectionPeriod
    <$> v .: "start"
    <*> v .: "end"

instance FromJSON Settings where
  parseJSON = withObject "Settings" $ \v -> Settings
    <$> v .: "database"
    <*> v .: "ldap"
    <*> v .: "server"
    <*> v .: "election"