module Main where

import Service.Service (app)
import Network.Wai.Handler.Warp (runSettings)
import Network.Wai.Handler.WarpTLS (runTLS)
import Data.Migrations (runMigrations)
import Data.Settings (Settings(..), Server(..))
import Data.Yaml (decodeFileThrow)

main :: IO ()
main = do
  config@Settings { server = Server { settings, tlsSettings } } <- decodeFileThrow "./settings.yaml"
  _ <- runMigrations config
  maybe (runSettings settings (app config)) (\tls -> runTLS tls settings (app config)) tlsSettings
