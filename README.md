# Election

## Back-end
### Build
Install [Stack](https://www.haskell.org/downloads#stack). Note that this also builds [PureScript](https://github.com/purescript/purescript/releases) and 
[PSC-package](https://github.com/purescript/psc-package/releases).

```bash
stack build
```

### Repl
```bash
stack ghci election:exe:election-exe
```

## Front-end
### Build
Note that the back-end build above already put the [PureScript](https://github.com/purescript/purescript/releases)
and [PSC-package](https://github.com/purescript/psc-package/releases) executables in `./.stack-work/install/**/bin`
so they can be run with `stack exec`.

```bash
stack exec psc-package build && ./bundle.sh
```

### Repl
Install [NodeJS](https://nodejs.org).

```bash
./repl.sh
```

## Run
The application can be run by executing the back-end executable. Before it can be executed the `static` folder and 
settings file need to be in the `cwd`.
The settings file must be named `settings.yaml` and could look like this:
```yaml
database:
  connection: "host='some hostname' port=5432 dbname='some database name' user='some database user' password='some database password'"
ldap:
  host: "some ldap hostname"
  port: 389
  # Security can be either Plain or TLS
  security: Plain
  # Optional bind DN (comment out if not needed). It is required when the LDAP server does not accept anonymous access.
  #bindDN: ""
  #bindPassword: ""
  baseDN: "some ldap base DN"
  fullNameAttribute: "cn"
  usernameAttribute: "uid"
server:
  port: 8080
  # Security can be either Plain or TLS
  security: Plain
  # The TLS certificate, must be commented in when using the TLS security setting; otherwise commented out.
  #certificate: ""
  #certificateKey: ""
election:
  # Start and end in format: yyyy-mm-ddThh:mm:ss[.sss]Z
  period:
    start: 2018-10-18T22:30:00Z
    end: 2018-10-19T22:30:00Z
```